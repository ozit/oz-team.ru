<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>

    <body class="">
        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-xxl">
                    <a class="navbar-brand" href="#">oz_it</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav me-auto">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Главная</a>
                            </li>
                        </ul>
                        <span class="navbar-text">Войти</span>
                    </div>
                </div>
            </nav>
        </header>

        <main class="main">
            @yield('content')
        </main>

        <footer>

        </footer>

        <script src="{{ asset('js/app.js') }}" defer></script>
    </body>
</html>
