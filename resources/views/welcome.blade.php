@extends('layouts.app')

@section('title', 'Сайт сообщества oz_it')

@section('content')
    <div class="index mt-4">
        <div class="container-xxl">
            <h1 class="mb-4">Ближайшие мероприятия</h1>
            <div class="index__news">
                <div class="row row-cols-1 row-cols-md-2 g-4">
                    <div class="col">
                        <div class="card card_type_index-feed">
                            <picture>
                                <source srcset="/images/mitap_web.webp" type="image/webp">
                                <source srcset="/images/mitap_web.jpg" type="image/jpeg">
                                <img class="card-img-top img-fluid" src="/images/mitap_web.jpg" alt="Митап">
                            </picture>

                            <div class="card-body">
                                <h2 class="card-title">Митап по web-разработке</h2>
                                <p class="card-text">
                                    Поговорим в целом про веб-разработку, какие направления популярные, какие языки, технологии, фреймворки используются. <br />
                                    Поговорим про laravel, react.js, 1c-bitrix
                                </p>
                                <p class="card-text mb-2">
                                    ⏰ Когда: 10 марта (чт), в 19:00 <br />
                                    📌 Где: г. Орехово-Зуево, ул. Ленина, 86, офис компании IS-ART <br />
                                    💰 Цена: бесплатно<br />
                                    🦶 Как добраться: <a href="https://yandex.ru/maps/-/CCUBMVgApA">Яндекс панорама</a>
                                </p>
                            </div>
                            <div class="card-footer">
                                <small class="text-muted">
                                    <a href="tg://resolve?domain=oz_it_club" class="text-decoration-none">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z"/>
                                        </svg>
                                        Чат в телеграмм (oz_it_club)
                                        </span>
                                    </a>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

